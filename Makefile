KUBERNETES_CONTEXT=kubernetes-admin@aletro.k8s
SSH_PRIVATE_KEY=~/.ssh/id_rsa
ANSIBLE_INVENTORY=env/inventory

use-context:
	kubectl config use-context $(KUBERNETES_CONTEXT)
	kubectl config current-context

# Hetzner Cloud Infrastructure

infrastructure-create:
	ansible-playbook infrastructure-create.yaml

infrastructure-destroy:
	ansible-playbook infrastructure-destroy.yaml

install-hcloud-provider:
	mkdir -p $GOPATH/src/github.com/hetznercloud
	cd $GOPATH/src/github.com/hetznercloud && git clone https://github.com/hetznercloud/terraform-provider-hcloud.git
	cd $GOPATH/src/github.com/hetznercloud/terraform-provider-hcloud && make build
	mkdir -p ~/.terraform/plugins
	cp $GOPATH/bin/terraform-provider-hcloud ~/.terraform/plugins/

#
# FIRST STEP: Kubernetes Infrastructure
#

kubernetes-step-1: setup-nodes kubeconfig

setup-nodes:
	ansible-playbook k8s-setup-nodes.yaml -i $(ANSIBLE_INVENTORY) --private-key $(SSH_PRIVATE_KEY)

kubeconfig:
	ansible-playbook k8s-export-kubeconfig.yaml -i $(ANSIBLE_INVENTORY) --private-key $(SSH_PRIVATE_KEY)

#
# SECOND STEP: update KUBECONFIG with downloaded kubeconfig, use pbcopy and configure tools like e.g. k8s Lens
#
# e.g. "export KUBECONFIG=~/.kube/kubernetes-admin-mycluster.k8s"

kubernetes-step-2: deploy-networking deploy-dns-helper

deploy-networking: use-context
	ansible-playbook k8s-deploy-networking.yaml -i $(ANSIBLE_INVENTORY)

deploy-dns-helper: use-context
	ansible-playbook k8s-deploy-dns-helper.yaml -i $(ANSIBLE_INVENTORY)

#
# THIRD STEP: wait until all system pods have been deployed successfully
#

kubernetes-step-3: deploy-monitoring deploy-ingress deploy-database

deploy-monitoring: use-context
	ansible-playbook k8s-deploy-monitoring.yaml -i $(ANSIBLE_INVENTORY)

deploy-ingress: use-context
	ansible-playbook k8s-deploy-ingress.yaml -i $(ANSIBLE_INVENTORY)

deploy-database: use-context
	ansible-playbook k8s-deploy-database.yaml -i $(ANSIBLE_INVENTORY)

#
# FOURTH STEP: wait until all infrastructure pods have been deployed successfully
#

kubernetes-step-4: deploy-nats

deploy-nats: use-context
	ansible-playbook k8s-deploy-nats.yaml -i $(ANSIBLE_INVENTORY)
