// DO NOT EDIT - ANSIBLE replace the variables

variable "hcloud_token" {
  type = string
  description = "Hetzner Cloud API Token - Replaced by Ansible Playbook on run"
}

variable "datacenter" {
  type = string
  description = "Datacenter Location - Replaced by Ansible Playbook on run"
}

variable "master_server_type" {
  type = string
  description = "Master Server Type - Replaced by Ansible Playbook on run"
}

variable "worker_server_type" {
  type = string
  description = "Worker Server Type - Replaced by Ansible Playbook on run"
}

variable "worker_count" {
  type = number
  description = "Worker count - Replaced by Ansible Playbook on run"
}

variable "floatip_count" {
  type = number
  description = "Floating IPs count - Replaced by Ansible Playbook on run"
}

variable "cluster_name" {
  type = string
  description = "Identifier for this Kubernetes cluster - Replaced by Ansible Playbook on run"
}

variable "domain" {
  type = string
  description = "Top Level Domain - Replaced by Ansible Playbook on run"
}

variable "network_name" {
  type = string
  description = "Private Network Name - Replaced by Ansible Playbook on run"
}

variable "network_ip_range" {
  type = string
  description = "Private Network IP range - Replaced by Ansible Playbook on run"
}

variable "subnet_master_ip_range" {
  type = string
  description = "Private Network IP Range for subnet of master nodes - Replaced by Ansible Playbook on run"
}

variable "subnet_master_ip" {
  type = string
  description = "Private Network IP prefix of master nodes - Replaced by Ansible Playbook on run"
}

variable "subnet_worker_ip_range" {
  type = string
  description = "Private Network IP Range for subnet of worker nodes - Replaced by Ansible Playbook on run"
}

variable "subnet_worker_ip" {
  type = string
  description = "Private Network IP prefix of worker nodes - Replaced by Ansible Playbook on run"
}

variable "os_image" {
  type = string
  description = "Image of operating system for servers - Replaced by Ansible Playbook on run"
}

